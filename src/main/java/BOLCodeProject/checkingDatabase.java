package BOLCodeProject;

import static BOLCodeProject.aboutDatabase.prepareDataForDatabase;
import java.io.BufferedReader;
import java.io.FileReader;

import java.io.IOException;
import java.util.ArrayList;
import org.json.JSONObject;  
import org.json.JSONArray;  

import org.springframework.web.client.RestTemplate;

import static BOLCodeProject.aboutDotSideFile.GeneratingSIDEFile;
import static BOLCodeProject.aboutDotSideFile.executingSeleniumInCommandOS;
import static BOLCodeProject.aboutDotSideFile.global;

import static com.google.protobuf.Any.parser;
import static com.google.protobuf.Api.parser;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import java.util.Calendar;

import org.json.JSONException;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

//@SpringBootApplication
public class checkingDatabase {
    
        static Global global = Main.global;
        
        private static final String filePath = "/home/autofsservice/API/src/main/config.json"; 

        static String [][] tempCollectValue1 = new String[100][100],collectValue1 = new String[100][100];
        static String [] tempCollectColumnName1 = new String[100],collectColumnName1 = new String[100];
        static String [][] tempCollectValue2 = new String[100][100],collectValue2 = new String[100][100];
        static String [] tempCollectColumnName2 = new String[100],collectColumnName2 = new String[100];

        public static void  readingCompanyIdFromTextFile(String key) throws SQLException, IOException{
        
                Connection connection = null;

                try {
 
                        // Load the MySQL JDBC driver

                        String driverName = "com.mysql.cj.jdbc.Driver";
                        Class.forName(driverName);

                        // Create a connection to the database
                        connection = DriverManager.getConnection( "jdbc:mysql://"+global.getAddress3()+"/"+global.getDatabase3()+"", ""+global.getUser3()+"", ""+global.getPassword3()+"" );
                         
                        System.out.println("Successfully Connected to the database!");

                }catch (ClassNotFoundException e) {

                        System.out.println("Could not find the database driver " + e.getMessage());

                }catch (SQLException e) {

                        System.out.println("Could not connect to the database " + e.getMessage());
                }
        
                try {

                        // Get a result set containing all data from test_table

                        Statement statement = connection.createStatement();
                        ResultSet results = statement.executeQuery("SELECT * from Queue where flag1 <2  and (status1 = 'waiting' OR status1 = 'unsuccess but BOL website is not locked') ORDER BY dateTime ASC limit 1");

                        while (results.next()) {

                                String theCompanyId = results.getString("CompanyId");
                                System.out.println("Fetching data by column name for row " + results.getRow() + " : " + theCompanyId);
                                String Id = results.getString("id");
                                System.out.println("ID: "+Id);
                                String year = results.getString("year");
                                System.out.println("year: "+year);

                                tempCollectValue1 = new String[100][100];
                                collectValue1 = new String[100][100];
                                tempCollectColumnName1 = new String[100];
                                collectColumnName1 = new String[100];

                                tempCollectValue2 = new String[100][100];
                                collectValue2 = new String[100][100];
                                tempCollectColumnName2 = new String[100];
                                collectColumnName2 = new String[100];

                                callingAPIForCheck(theCompanyId,key,tempCollectValue1,collectValue1,tempCollectColumnName1,collectColumnName1,tempCollectValue2,collectValue2,tempCollectColumnName2,collectColumnName2,Id,year);

                        } 
                } catch (SQLException e) {
 
                        System.out.println("Could not retrieve data from the database " + e.getMessage());
        
                }
        } 
        
        public static void callingAPIForCheck(String theCompanyId,String key,String[][] tempCollectValue1,String[][] collectValue1,String[] tempCollectColumnName1,String[] collectColumnName1,String[][] tempCollectValue2,String[][] collectValue2,String[] tempCollectColumnName2,String[] collectColumnName2, String Id, String year) throws IOException, JSONException {

                String C = theCompanyId;

                final String url = "http://localhost:8080/1/"+year+"/"+C+""; 

                HttpHeaders headers = new HttpHeaders();
                headers.add("Authorization", key);

                HttpEntity request = new HttpEntity(headers);
                ResponseEntity<String> result = new RestTemplate().exchange(url, HttpMethod.GET, request, String.class);

                System.out.println("URL path call API: "+url);
                System.out.println("The result: "+result);

                String json = result.getBody();

                JSONArray array1 = new JSONArray("["+json+"]");  
        
                for(int count=0; count < array1.length(); count++){  

                        JSONObject object1 = array1.getJSONObject(count);  

                        System.out.println("CHECKING DATABASE: callingAPIForCheck");

                        if(object1.getString("companyId") == "" && object1.getString("year") == ""){
                
                                System.out.println("GeneratingSIDEFile");

                                GeneratingSIDEFile(theCompanyId,tempCollectValue1,collectValue1,tempCollectColumnName1,collectColumnName1,tempCollectValue2,collectValue2,tempCollectColumnName2,collectColumnName2); //1

                                System.out.println("Company Id "+theCompanyId+"is sending to executingSeleniumInCommandOS");

                                executingSeleniumInCommandOS  (theCompanyId,tempCollectValue1,collectValue1,tempCollectColumnName1,collectColumnName1,tempCollectValue2,collectValue2,tempCollectColumnName2,collectColumnName2,Id); //2

                                System.out.println("End of CHECKING DATABASE: callingAPIForCheck");
                                System.out.println("=============================================");
                                System.out.println("");
                
                        }
                }  
        }
}
