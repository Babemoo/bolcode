package BOLCodeProject;

import java.io.*;
import java.util.Arrays;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

import static BOLCodeProject.aboutDatabase.prepareDataForDatabase;


public class RegularExpression {
    
        static int tempJ=0,j=1,statusCountColume=0,statusCorrect =0,callComponent = 0;;

        public static void cuttingValue(String line, String companyId,String[][] tempCollectValue1,String[][] collectValue1,String[] tempCollectColumnName1,String[] collectColumnName1,String[][] tempCollectValue2,String[][] collectValue2,String[] tempCollectColumnName2,String[] collectColumnName2){
     
                String pattern = "(.*?)(\\d+.*|-.*)";

                if(callComponent == 0){
                        int i=0;

                        try {

                                System.out.println("the begining of REG EX: cuttingValue : "+line);

                                Pattern r = Pattern.compile(pattern);

                                Matcher m = r.matcher(line);
                                m.find();

                                String S = m.group(1).replaceFirst("\\s+","");

                                S= S.replaceAll("\\s+$", "");

                                tempCollectColumnName1[tempJ]= S;

                                if(tempJ >=7 ||  statusCorrect==1){

                                        j++;
                                        collectColumnName1[j] =  tempCollectColumnName1[tempJ];

                                }

                                String[] output = m.group(2).split("\\s+");

                                for (String retval : output) {

                                        tempCollectValue1[tempJ][i]= retval;

                                        System.out.print("T E M P "+tempCollectValue1[tempJ][i]+"temp "+tempJ+"i "+i);

                                        if(tempJ == 0){

                                                collectValue1[0][2]= companyId;
                                                System.out.println("collectValue["+j+"]["+i+"] "+collectValue1[j][i]); 
                                                System.out.println ("C O M P A N Y :"+companyId);

                                        }

                                        if(tempJ >=2 && tempJ <7 && i==2 ){

                                                String s = tempCollectValue1[tempJ][2];
                                                int count = 0;
                                                
                                                for (int a = 0, len = s.length(); a < len; a++) {
                                                        if (Character.isDigit(s.charAt(a))) {
                                                                count++;
                                                        }
                                                } 

                                                if(count ==4){
                                                    
                                                        collectValue1[j][i]=tempCollectValue1[tempJ][2];
                                                        System.out.println("collectValue["+j+"]["+i+"] "+collectValue1[j][i]);
                                                        j++;

                                                        collectValue1[j][i]=tempCollectValue1[tempJ][0]+"/"+tempCollectValue1[tempJ][1]+"/"+tempCollectValue1[tempJ][2];

                                                        statusCountColume++;
                                                        System.out.println("statusCountColume: "+statusCountColume);

                                                        j++;
                                                }
                                        }

                                        if((tempJ ==2 && statusCountColume != 1)||(tempJ ==3 && statusCountColume != 2)||(tempJ ==4 && statusCountColume != 3)||(tempJ ==5 && statusCountColume != 4)||(tempJ ==6 && statusCountColume != 5)){
                                                
                                                collectValue1[j][i]=tempCollectValue1[tempJ][i];
                                                System.out.println("collectValue["+j+"]["+i+"] "+collectValue1[j][i]);

                                                collectColumnName1[j] =  tempCollectColumnName1[tempJ];
                                                statusCorrect =1 ;
                                        }

                                        if(tempJ >=7){

                                                collectValue1[j][i]=tempCollectValue1[tempJ][i];
                                                System.out.println("collectValue["+j+"]["+i+"] "+collectValue1[j][i]);

                                                collectColumnName1[j] =  tempCollectColumnName1[tempJ];

                                        }
                                        
                                        i++;
                                }

                                tempJ++;

                                System.out.println("the end of REG EX: cuttingValue");

                                if(tempCollectColumnName1[tempJ-1].equals("รวมหนิ้สินและส่วนของผู้ถือหุ้น")){

                                        tempJ=0;

                                        j=1;

                                        statusCorrect=0;
                                        statusCountColume=0;

                                        System.out.println("sending data to ABOUT DATABASE: prepareDataForDatabase");

                                        prepareDataForDatabase(collectValue1,collectColumnName1);

                                        callComponent = 1;
                                }

                        }catch (Exception error) {

                                System.out.println("Log Error in REG EX: cuttingValue"+error);

                        }
                }
                
                if(callComponent == 1){
            
                        int i=0;

                        try {

                                System.out.println("the begining of REG EX: cuttingValue : "+line);

                                Pattern r = Pattern.compile(pattern);

                                Matcher m = r.matcher(line);
                                m.find();

                                String S = m.group(1).replaceFirst("\\s+","");

                                S= S.replaceAll("\\s+$", "");

                                tempCollectColumnName2[tempJ]= S;

                                if(tempJ >=7 ||  statusCorrect==1){

                                        j++;

                                        collectColumnName2[j] =  tempCollectColumnName2[tempJ];

                                }
                                
                                String[] output = m.group(2).split("\\s+");

                                System.out.println("output: "+Arrays.toString(output));

                                for (String retval : output) {

                                        tempCollectValue2[tempJ][i]= retval;

                                        System.out.print("T E M P "+tempCollectValue2[tempJ][i]+"temp "+tempJ+"i "+i);

                                        if(tempJ == 0){

                                                collectValue2[0][2]= companyId;
                                                System.out.println("collectValue["+j+"]["+i+"] "+collectValue2[j][i]); 
                                                System.out.println ("C O M P A N Y :"+companyId);

                                        }

                                        if(tempJ >=2 && tempJ <7 && i==2 ){

                                                String s = tempCollectValue2[tempJ][2];
                                                int count = 0;
                                                
                                                for (int a = 0, len = s.length(); a < len; a++) {
                                                        if (Character.isDigit(s.charAt(a))) {
                                                                count++;
                                                        }
                                                } 

                                                if(count ==4){

                                                        collectValue2[j][i]=tempCollectValue2[tempJ][2];
                                                        System.out.println("collectValue["+j+"]["+i+"] "+collectValue2[j][i]);
                                                        j++;

                                                        collectValue2[j][i]=tempCollectValue2[tempJ][0]+"/"+tempCollectValue2[tempJ][1]+"/"+tempCollectValue2[tempJ][2];

                                                        statusCountColume++;
                                                        System.out.println("statusCountColume: "+statusCountColume);

                                                        j++;

                                                }
                                        }

                                        if((tempJ ==2 && statusCountColume != 1)||(tempJ ==3 && statusCountColume != 2)||(tempJ ==4 && statusCountColume != 3)||(tempJ ==5 && statusCountColume != 4)||(tempJ ==6 && statusCountColume != 5)){
                                                
                                                collectValue2[j][i]=tempCollectValue2[tempJ][i];
                                                System.out.println("collectValue["+j+"]["+i+"] "+collectValue2[j][i]);

                                                collectColumnName2[j] =  tempCollectColumnName2[tempJ];
                                                statusCorrect =1 ;
                                        }

                                        if(tempJ >=7){

                                                collectValue2[j][i]=tempCollectValue2[tempJ][i];
                                                System.out.println("collectValue["+j+"]["+i+"] "+collectValue2[j][i]);

                                                collectColumnName2[j] =  tempCollectColumnName2[tempJ];

                                        }
                                        
                                        i++;
                                }

                                tempJ++;

                                System.out.println("the end of REG EX: cuttingValue");

                                if(tempCollectColumnName2[tempJ-1].equals("จำนวนหุ้นสามัญถัวเฉลี่ยถ่วงน้ำหนัก")){

                                        tempJ=0;
                                        j=1;
                                        statusCorrect=0;
                                        statusCountColume=0;

                                        System.out.println("sending data to ABOUT DATABASE: prepareDataForDatabase");

                                        prepareDataForDatabase(collectValue2,collectColumnName2);

                                        callComponent = 0;
                                }

                        }catch (Exception error) {

                                System.out.println("Log Error in REG EX: cuttingValue"+error);

                        }
                }
        }
}