package BOLCodeProject;

import java.io.IOException;
import BOLCodeProject.aboutDotSideFile;
import static BOLCodeProject.aboutDotSideFile.executingSeleniumInCommandOS;


import static BOLCodeProject.checkingDatabase.readingCompanyIdFromTextFile;

import java.nio.file.Files;
import java.nio.file.Paths;
        
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.*;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

public class Main {
    
        public static Global global = new Global();
    
        public static void main(String[] args) throws IOException, Exception {
     
                System.out.println("It's in a Main class");

                // call load function
                if (!global.load()) {
                        System.out.println("Loading Error: " + global.getError());
                        return;
	}
                
                String file = "/home/autofsservice/API/src/main/config.json";
                String json = readFileAsString(file);

                Object obj=JSONValue.parse(json);     
                JSONObject jsonObject = (JSONObject) obj; 
                String key = (String) jsonObject.get("key"); 

                System.out.println("Sending key to CHECKING DATABASE: readingCompanyIdFromTextFile");
                readingCompanyIdFromTextFile(key);
        
        }
        
        public static String readFileAsString(String file)throws Exception{
                return new String(Files.readAllBytes(Paths.get(file)));
        }
}
