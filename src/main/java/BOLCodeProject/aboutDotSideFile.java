package BOLCodeProject;

import java.io.File;
import java.io.IOException;
import java.io.*;
import java.awt.Desktop;
import java.nio.file.Files;
import java.util.Scanner;
import java.io.BufferedReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;

import static BOLCodeProject.RegularExpression.cuttingValue;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import java.util.concurrent.TimeUnit;

public class aboutDotSideFile {
    
        static Global global = Main.global;
        
        public static void GeneratingSIDEFile(String theCompanyId,String[][] tempCollectValue1,String[][] collectValue1,String[] tempCollectColumnName1,String[] collectColumnName1,String[][] tempCollectValue2,String[][] collectValue2,String[] tempCollectColumnName2,String[] collectColumnName2)throws IOException{

                String sourceFileName1 = "/home/autofsservice/BOLCodeProject/Begin.side";
                String sourceFileName2 = "/home/autofsservice/BOLCodeProject/Last.side";
                String destinationFileName = "/home/autofsservice/BOLCodeProject/BolSideFile.side";

                BufferedReader br1 = null,br2 = null;
                PrintWriter pw = null; 

                try {

                        br1 = new BufferedReader(new FileReader( sourceFileName1, StandardCharsets.UTF_8));
                        br2 = new BufferedReader(new FileReader( sourceFileName2, StandardCharsets.UTF_8 ));
                        pw =  new PrintWriter(new FileWriter( destinationFileName, StandardCharsets.UTF_8 ));

                        String line;

                        while ((line = br1.readLine()) != null) {
                                pw.println(line);
                        }

                        pw.println("      \"value\": \""+theCompanyId+"\"");

                        while ((line = br2.readLine()) != null) {
                                pw.println(line);
                        }

                        FileWriter fw = new FileWriter(destinationFileName, true);

                        br1.close();
                        br2.close();
                        pw.close();

                }catch (Exception e) {
                        e.printStackTrace();
                }
        }
    
        public  static void executingSeleniumInCommandOS (String companyId,String[][] tempCollectValue1,String[][] collectValue1,String[] tempCollectColumnName1,String[] collectColumnName1,String[][] tempCollectValue2,String[][] collectValue2,String[] tempCollectColumnName2,String[] collectColumnName2, String Id) throws IOException {
        
                String s;
                int statusProcess = 0, statusWeb = 0;;

                try {
                    
                        Connection con = DriverManager.getConnection( "jdbc:mysql://"+global.getAddress3()+"/"+global.getDatabase3()+"", ""+global.getUser3()+"", ""+global.getPassword3()+"" );
                               
                        Statement stmt = null;

                        TimeUnit.SECONDS.sleep(5);

                        ProcessBuilder processBuilder = new ProcessBuilder();
                        processBuilder.command("bash", "-c", "selenium-side-runner -c \"browserName=chrome chromeOptions.args=[disable-infobars, headless]\" /home/autofsservice/BOLCodeProject/BolSideFile.side");
                        Process p = processBuilder.start();
                        p.waitFor();

                        StringBuilder output = new StringBuilder(); 
                        BufferedReader br = new BufferedReader(new InputStreamReader(p.getInputStream())); 

                        System.out.println("ABOUT .SIDE FILE: executingSeleniumInCommandOS");

                        while ((s = br.readLine()) != null){
                            
                                System.out.println("line :  "+s);
                   
                                try{

                                    System.out.println("Input "+s);

                                    cuttingValue(s,companyId,tempCollectValue1,collectValue1,tempCollectColumnName1,collectColumnName1,tempCollectValue2,collectValue2,tempCollectColumnName2,collectColumnName2);//line by line

                                    System.out.println("Finish Sending data to cuttingValue");

                                }catch(Exception e){

                                    System.out.println("PROCESS1 Have an error from ABOUT .SIDE FILE: executingSeleniumInCommandOS "+e);

                                }
                        }
            
                
                        System.out.println("The COMPANYID "+companyId);
                        System.out.println ("p.exitValue of Process1: " + p.exitValue());
            
//=====================Exit Completely======================================= 
           
                        if(p.exitValue() == 0){
                
                                try {

                                        System.out.println("Connected database successfully...");

                                        //Execute a query
                                        stmt = con.createStatement();

                                        String sql = "UPDATE Queue SET status1 = 'success' , flag1 = flag1+1 WHERE (CompanyId = '"+companyId+"' and id = '"+Id+"')";
                                        stmt.executeUpdate(sql);
                                        System.out.println("Success UPDATE Success!");

                                        //con.close();

                                }catch(SQLException ex) {

                                    System.out.println("SQLException: " + ex.getMessage());
                                    System.out.println("SQLState: " + ex.getSQLState());
                                    System.out.println("VendorError: " + ex.getErrorCode());

                                }
                        }
 //====================Exit with problem====================================== 
            
                        if(p.exitValue() > 0  || p.exitValue() < 0 ){
                            
                                statusProcess =1;
                
                                try {
                   
                                        System.out.println("Connected database successfully...");

                                        //Execute a query
                                        stmt = con.createStatement();

                                        String sql = "UPDATE Queue SET status1 = 'unsuccess but BOL website is not locked', flag1 = flag1+1 WHERE (CompanyId = '"+companyId+"' and id = '"+Id+"')";
                                        stmt.executeUpdate(sql);
                                        System.out.println("ID: "+Id);
                                        System.out.println("Success UPDATE Unsuccess!");

                                        //con.close();

                                }catch(SQLException ex) {

                                        System.out.println("SQLException: " + ex.getMessage());
                                        System.out.println("SQLState: " + ex.getSQLState());
                                        System.out.println("VendorError: " + ex.getErrorCode());

                                }
                        }
            
                        p.destroy();
                        System.out.println("Destroy Process1");
            
                        if(p.isAlive()){
                                p.destroyForcibly();
                                System.out.println("Force Destroy Process1");
                        }
            
//====================Check if BOL Website is locked==========================
            
                        if(statusProcess == 1){
                                for(int i=0;i<10;i++){

                                        TimeUnit.SECONDS.sleep(1);
                                        System.out.println(i+" second passed");

                                }

                                try {

                                        ProcessBuilder processBuilder2 = new ProcessBuilder();
                                        processBuilder2.command("bash", "-c", "selenium-side-runner -c \"browserName=chrome chromeOptions.args=[disable-infobars, headless]\" ~/BOLCodeProject/bolLogin.side");
                                        Process p2 = processBuilder2.start();
                                        p2.waitFor();

                                        StringBuilder output2 = new StringBuilder(); 

                                        System.out.println ("p2.exitValue of Process1: " + p2.exitValue());
                
                                        if(p2.exitValue() > 0 || p2.exitValue() < 0){
                                            
                                                statusWeb =1;
                                                
                                                try {

                                                        System.out.println("Connected database successfully...");

                                                        //Execute a query
                                                        stmt = con.createStatement();

                                                        String sql = "UPDATE Queue SET status1 = 'unsuccess and BOL website is locked!', flag1 = flag1+1 WHERE (CompanyId = '"+companyId+"' and id = '"+Id+"')";
                                                        stmt.executeUpdate(sql);
                                                        System.out.println("ID: "+Id);
                                                        System.out.println("Success UPDATE Unsuccess!");

                                                        //con.close();

                                                }catch(SQLException ex) {

                                                        System.out.println("SQLException: " + ex.getMessage());
                                                        System.out.println("SQLState: " + ex.getSQLState());
                                                        System.out.println("VendorError: " + ex.getErrorCode());

                                                }
                        
                                        }else if (p2.exitValue() == 0){
                                            
                                                statusWeb =0;
                                                
                                                try {
                                                        System.out.println("Connected database successfully...");

                                                        //Execute a query
                                                        stmt = con.createStatement();

                                                        String sql = "UPDATE Queue SET status1 = 'unsuccess but BOL website is not locked', flag1 = flag1+1 WHERE (CompanyId = '"+companyId+"' and id = '"+Id+"')";
                                                        stmt.executeUpdate(sql);
                                                        System.out.println("ID: "+Id);
                                                        System.out.println("Success UPDATE Unsuccess!");

                                                        //con.close();

                                                }catch(SQLException ex) {

                                                        System.out.println("SQLException: " + ex.getMessage());
                                                        System.out.println("SQLState: " + ex.getSQLState());
                                                        System.out.println("VendorError: " + ex.getErrorCode());

                                                }
                        
                                                DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");  

                                                LocalDateTime now = LocalDateTime.now();  

                                                String textToAppend = "\r\nAt "+dtf.format(now)+": This "+companyId+" is fail to get data from bol website!"; //new line in content

                                                Path path = Paths.get("/home/autofsservice/BOLCodeProject/logUnsuccessCompany.txt");

                                                Files.write(path, textToAppend.getBytes(), StandardOpenOption.APPEND);  //Append mode

                                                System.out.println("Successfully wrote the error company Id to log file1.");
                                        }
                    
                                        p2.destroy();
                                        System.out.println("Destroy Process2");
                        
                                        if(p2.isAlive()){
                                            
                                                p2.destroyForcibly();
                                                System.out.println("Force Destroy Process2");
                                        }
                                }catch(Exception e){
                                    
                                        System.out.println("PROCESS2 Have an error from ABOUT .SIDE FILE: executingSeleniumInCommandOS "+e);
                                }
                   
                        }
                   
//===================BOL Website is locked!==================================
                
                         if(statusProcess == 1 && statusWeb == 1){
                    
                                try {

                                        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");  

                                        LocalDateTime now = LocalDateTime.now();  

                                        String textToAppend = "\r\nAt "+dtf.format(now)+": This "+companyId+" is failed to get data from bol website!"; //new line in content

                                        Path path = Paths.get("/home/autofsservice/BOLCodeProject/logUnsuccessCompany.txt");

                                        Files.write(path, textToAppend.getBytes(), StandardOpenOption.APPEND);  //Append mode

                                        System.out.println("Successfully wrote the error company Id to log file.");

                    
                                }catch (IOException e) {

                                        System.out.println("An error occurred when trying to write error into log file");
                                        e.printStackTrace();

                                }
                
//===================Handle with sleep for 10 minutes===============================

                                System.out.println("Wait for Signin in 10 munites");

                                for(int i=0;i<600;i++){

                                        TimeUnit.SECONDS.sleep(1);
                                        System.out.println(i+" second passed");

                                }
//=============Try to Sign in again after sleep for 5 minutes===============

                                try{
                                        int count =0, statusLocked =1, statusExit=0;

                                        while(count<3 & statusLocked ==1){

                                                ProcessBuilder processBuilder3 = new ProcessBuilder();
                                                processBuilder3.command("bash", "-c", "selenium-side-runner -c \"browserName=chrome chromeOptions.args=[disable-infobars, headless]\" ~/BOLCodeProject/bolLogin.side");
                                                Process p3 = processBuilder3.start();
                                                p3.waitFor();

                                                StringBuilder output3 = new StringBuilder();

                                                System.out.println ("p3.exitValue of Process1: " + p3.exitValue());

                                                if(p3.exitValue() > 0 || p3.exitValue() < 0){

                                                        statusExit = 1;

                                                }else if(p3.exitValue() == 0){

                                                        statusExit = 0;

                                                        try {

                                                                System.out.println("Connected database successfully...");

                                                                //Execute a query
                                                                stmt = con.createStatement();

                                                                String sql = "UPDATE Queue SET status1 = 'unsuccess but BOL website is not locked', flag1 = flag1+1 WHERE (CompanyId = '"+companyId+"' and id = '"+Id+"')";
                                                                stmt.executeUpdate(sql);
                                                                System.out.println("ID: "+Id);
                                                                System.out.println("Success UPDATE Unsuccess!");

                                                                //con.close();

                                                        }catch(SQLException ex) {

                                                                System.out.println("SQLException: " + ex.getMessage());
                                                                System.out.println("SQLState: " + ex.getSQLState());
                                                                System.out.println("VendorError: " + ex.getErrorCode());

                                                        }
                                                }

                                                p3.destroy();
                                                System.out.println("Destroy Process3");

                                                if(p3.isAlive()){
                                                        p3.destroyForcibly();
                                                        System.out.println("Force Destroy Process3");
                                                }

        //=============Check if it's still be locked=================================

                                                if(statusExit == 1){ 

                                                        System.out.println("Still Lock!!!");
                                                        
                                                        for(int i=0;i<300;i++){

                                                                TimeUnit.SECONDS.sleep(1);
                                                                System.out.println(i+" second passed");

                                                        }

                                                        try {
                                                            
                                                                System.out.println("Connected database successfully...");

                                                                //Execute a query
                                                                stmt = con.createStatement();

                                                                String sql = "UPDATE Queue SET status1 = 'unsuccess but BOL website is not locked' WHERE (CompanyId = '"+companyId+"' and id = '"+Id+"')";
                                                                stmt.executeUpdate(sql);
                                                                System.out.println("ID: "+Id);
                                                                System.out.println("Success UPDATE Unsuccess!");

                                                                //con.close();

                                                         }catch(SQLException ex) {

                                                                System.out.println("SQLException: " + ex.getMessage());
                                                                System.out.println("SQLState: " + ex.getSQLState());
                                                                System.out.println("VendorError: " + ex.getErrorCode());

                                                        }
                                                        statusExit = 0;
                                                }
                                                count++;
                                        }
                                        statusWeb = 0;

                                }catch (Exception e) {
                    
                                        System.out.println("An error occurred when trying process 3!");
                                        System.out.println(e);
                    
                                }
                        }
                        con.close();
  
                } catch (Exception e) {  //BOL Website is not locked but has some accident=========================================
                        System.out.println("An error occurred");
                        System.out.println(e);
                }
        } 
}