package BOLCodeProject;

import static BOLCodeProject.RegularExpression.callComponent;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Arrays;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import java.sql.*;
import static java.time.LocalDateTime.now;

public class aboutDatabase {
    
        static Global global = Main.global;
        
        public static void prepareDataForDatabase(String[][] collectValue,String[] collectColumnName) {
        
                if(callComponent == 0){
                        int j=-2;

                        for(int i=0;i<=4;i++){

                                int jj=i+1;

                                System.out.println("===================="); 
                                System.out.println("ปีที่ "+jj);

                                String[] EnglishColumnNameInDatabase={"companyId","year","date","cashAndDeposit","accountRecv","accountRecvAndNoteRecv","totalShortTermLoanRecv","inventory","otherCurrentAssets","totalCurAsset","loanAndInvestment","propPlantEquip","otherNonCurrentAssets","totalNonCurrentAssets","totalAsset","bankOdAndLoan","accountPayable","totalAccPayableAndBillPaid","totalShortTermLoan","otherCurLib","totalCurLib","longTermLoan","otherLib","totalNonCurLib","totalLib","authorizedPreferShare","authorizedShareCapt","paidInCaptPreferShare","paidInCaptCommonStk","retainedEarnings","other","totalEquity","totalLibAndEquity"}; 
                                String[] ThaiColumnName= {"หมายเลขบริษัท","ปี","วันที่","เงินสดและเงินฝากสถาบันการเงิน","ลูกหนี้การค้า","ลูกหนี้และตั๋วเงินรับทางการค้า สุทธิ","รวมเงินให้กู้ยืมระยะสั้น","สินค้าคงเหลือสุทธิ","สินทรัพย์หมุนเวียนอื่น","รวมสินทรัพย์หมุนเวียน","รวมเงินให้กู้ยืมและเงินลงทุนระยะยาว","ที่ดิน อาคารและอุปกรณ์ สุทธิ","สินทรัพย์ไม่หมุนเวียนอื่น","รวมสินทรัพย์ไม่หมุนเวียน","รวมสินทรัพย์","เงินเบิกเกินบัญชีและเงินกู้ยืมระยะสั้นจากสถาบันการเงิน","เจ้าหนี้การค้า","รวมเจ้าหนี้การค้าและตั๋วเงินจ่าย","รวมเงินกู้ยืมระยะสั้น","หนี้สินหมุนเวียนอื่น","รวมหนี้สินหมุนเวียน","รวมเงินกู้ยืมระยะยาว","หนี้สินไม่หมุนเวียนอื่น","รวมหนี้สินไม่หมุนเวียน","รวมหนี้สิน","ทุนจดทะเบียน – หุ้นบุริมสิทธิ์" ,"ทุนจดทะเบียน","ทุนที่ออกและชำระแล้ว – หุ้นบุริมสิทธิ์","ทุนที่ออกและชำระแล้ว – หุ้นสามัญ" ,"กำไร (ขาดทุน)สะสม" ,"รายการอื่น","รวมส่วนของผู้ถือหุ้น","รวมหนิ้สินและส่วนของผู้ถือหุ้น"};
                                String[] Value = new String[33];

                                try{
                                        j=j+3;
                                        Value[0]=("\""+collectValue[0][2]+"\"");
                                        System.out.println("Value[0] collectValue[0][2] "+collectValue[0][2]);

                                        Value[1]=("\""+collectValue[j][2]+"\"");
                                        System.out.println("Value[1] collectValue["+j+"][2] "+collectValue[j][2]);

                                        Value[2]=("\""+collectValue[j+1][2]+"\"");
                                        System.out.println("Value[2] collectValue["+(j+1)+"][2] "+collectValue[j+1][2]);

                                        for(int countj=0;countj<50;countj++){
                                                for(int k=0;k<=4;k++){
                                                        System.out.println("collectValue["+countj+"]["+k+"] "+collectValue[countj][k]);
                                                }
                                        }

                                        for(int counti=0;counti<ThaiColumnName.length;counti++){
                                                for(int countj=0;countj<50;countj++){
                                                        if(ThaiColumnName[counti].equals(collectColumnName[countj])){

                                                                Value[counti]=("\""+collectValue[countj][i]+"\"");
                                                                System.out.println("Value["+counti+"] "+Value[counti]+ "collectValue["+countj+"]["+i+"] "+collectValue[countj][i]);

                                                        }
                                                }
                                        }

                                        for(int count=0;count<Value.length;count++){
                                                if(Value[count]== null){
                                                        Value[count] = "\"\"";
                                                }
                                        }

                                        sendingDataToDatabase(Value, EnglishColumnNameInDatabase);

                                }catch(Exception error) {

                                        System.out.println("Error from ABOUT DATABASE: prepareDataForDatabase "+error);
                                }
                        }
                }

                if(callComponent == 1){

                        int j=-2;

                        for(int i=0;i<=4;i++){

                                int jj=i+1;

                                System.out.println("===================="); 
                                System.out.println("ปีที่ "+jj);

                                String[] EnglishColumnNameInDatabase={"companyId","year","date","totalrevenue","otherincome","costgoodsold","grossprofit","operateexpense","ebit","otherexpense","incomebeforetax","interest","incometax","netprofit"};
                                String[] ThaiColumnName= {"หมายเลขบริษัท","ปี","วันที่","รายได้จากการขายและบริการ – สุทธิ","รวมรายได้อื่น","ต้นทุนขาย และ/หรือบริการ","กำไร(ขาดทุน)ขั้นต้น","รวมค่าใช้จ่ายในการดำเนินงาน","กำไร(ขาดทุน)จากการดำเนินงาน","ค่าใช้จ่ายอื่น","กำไร (ขาดทุน) ก่อนดอกเบี้ยและภาษีเงินได้","ดอกเบี้ยจ่าย","ภาษีเงินได้","กำไร(ขาดทุน) สุทธิ"};
                                String[] Value = new String[14];

                                try{
                                        j=j+3;
                                        Value[0]=("\""+collectValue[0][2]+"\"");
                                        System.out.println("Value[0] collectValue[0][2] "+collectValue[0][2]);
                                        Value[1]=("\""+collectValue[j][2]+"\"");
                                        System.out.println("Value[1] collectValue["+j+"][2] "+collectValue[j][2]);



                                        Value[2]=("\""+collectValue[j+1][2]+"\"");
                                        System.out.println("Value[2] collectValue["+(j+1)+"][2] "+collectValue[j+1][2]);

                                        for(int countj=0;countj<50;countj++){
                                               for(int k=0;k<=4;k++){
                                                        System.out.println("collectValue["+countj+"]["+k+"] "+collectValue[countj][k]);
                                                }
                                        }

                                        for(int counti=0;counti<ThaiColumnName.length;counti++){
                                                for(int countj=0;countj<50;countj++){
                                                        if(ThaiColumnName[counti].equals(collectColumnName[countj])){
                                                                Value[counti]=("\""+collectValue[countj][i]+"\"");
                                                                System.out.println("Value["+counti+"] "+Value[counti]+ "collectValue["+countj+"]["+i+"] "+collectValue[countj][i]);
                                                        }
                                                }
                                        }

                                        for(int count=0;count<Value.length;count++){
                                                if(Value[count]== null){
                                                        Value[count] = "\"\"";
                                                }
                                        }

                                        sendingDataToDatabase(Value, EnglishColumnNameInDatabase);

                                }catch(Exception error) {
                                        System.out.println("Error from ABOUT DATABASE: prepareDataForDatabase "+error);
                                }
                        } 
                }
        }
    
        public static void sendingDataToDatabase(String[] Value,String[] EnglishColumnNameInDatabase) {
        
                try {
             
                        Connection con = null;
                        Statement stmt = null;
            
                        if(callComponent == 0){
                
                                //Open a connection
                                con = DriverManager.getConnection( "jdbc:mysql://"+global.getAddress1()+"/"+global.getDatabase1()+"", ""+global.getUser1()+"", ""+global.getPassword1()+"" );
                                
                                System.out.println("Connected database successfully...");

                                //Execute a query
                                System.out.println("Creating table in given database...");

                                stmt = con.createStatement();

                                String joinedColumns = String.join(",", EnglishColumnNameInDatabase);
                                String joinedValue = String.join(",", Value);

                                String sql = "INSERT INTO boltable ("+joinedColumns+") VALUES ("+joinedValue+") ";

                                stmt.executeUpdate(sql);
                                System.out.println("Uploading Success!");
                
                        }else if(callComponent == 1){
                
                                //Open a connection
                                con = DriverManager.getConnection( "jdbc:mysql://"+global.getAddress2()+"/"+global.getDatabase2()+"", ""+global.getUser2()+"", ""+global.getPassword2()+"" );
                                System.out.println("Connected database successfully...");

                                //Execute a query
                                System.out.println("Creating table in given database...");

                                stmt = con.createStatement();

                                String joinedColumns = String.join(",", EnglishColumnNameInDatabase);
                                String joinedValue = String.join(",", Value);

                                String sql = "INSERT INTO bol2table ("+joinedColumns+") VALUES ("+joinedValue+") ";

                                stmt.executeUpdate(sql);
                                System.out.println("Uploading Success!");

                        }else{
                            
                                System.out.println("no error");
                        }
            
                        con.close();

                }catch(SQLException ex) {
                
                        // handle any errors
                        System.out.println("SQLException: " + ex.getMessage());
                        System.out.println("SQLState: " + ex.getSQLState());
                        System.out.println("VendorError: " + ex.getErrorCode());
            
                }
        }
}